﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("请输入车名");
            car.Carname = Console.ReadLine();
            Console.WriteLine("请输入车的颜色");
            car.Carcolor = Console.ReadLine();
            Console.WriteLine("请输入车轮数");
            car.Carwheel = int.Parse(Console.ReadLine());
            car.Run();
        }
    }
}
