﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pr2
{
    public abstract class Animals
    {
        public string name { get; set; }

        public string color { get; set; }

        public void Mobile()
        {
            Console.WriteLine();
            Console.WriteLine("动物都是会移动的");
        }
    }
}
