﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pr1
{
    public class Car : Garage
    {
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int CarWheel { get; set; }


        public void Run()
        {


            if (CarWheel >= 4)
            {
                Console.WriteLine("车子可以起跑");
            }


            else
            {
                Garage.Repair();
                Console.WriteLine("请输入修车厂的名字");
                GarageName = Console.ReadLine();

                Console.WriteLine("请输入修车厂的地址");
                GarageAdd = Console.ReadLine();

                Console.WriteLine("请输入修车厂的电话");
                GaragePhone = int.Parse(Console.ReadLine());

            }
            Console.WriteLine("结束");
        }
    }
}
