﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Car
    {
        public string Name { set; get; }
        public string Color { set; get; }
        public int Wheel { set; get; }
       
        public virtual void Print()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine("{0}的{1}有{2}个轮子，可以正常行驶",Color,Name,Wheel);
            }
            else
            {
                Console.WriteLine( "车子损坏，需送厂维修" );
            }
        }

    }
    
}
