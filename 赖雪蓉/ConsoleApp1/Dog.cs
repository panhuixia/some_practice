﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Dog : Animals
    {
        public override void Name()
        {
            Console.WriteLine("狗狗叫贝贝");
        }
        public override void Color()
        {
            Console.WriteLine("狗狗是白色的");
        }
        public override void Move()
        {
            Console.WriteLine("狗狗会跑");
        }
        public void Bite()
        {
            Console.WriteLine("狗狗会咬人");
        }
    }
}
