﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
     class Car
    {
        public static  string Name { get; set; }
        public static  string Color { get; set; }
        public int Wheel { get; set; }

        public virtual void Action()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine("{0}的{1}有{2}个轮子，可以正常跑", Color, Name, Wheel);
            }
            else
            {
                Console.WriteLine("{0}的{1}损坏了，需要送去修车厂", Color, Name);
            }
        }

    }
}
