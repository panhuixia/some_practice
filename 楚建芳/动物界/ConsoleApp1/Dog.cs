﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Dog:Animal
    {
        public string PersonName { get; set; }

        public override void Action()
        {
            Console.WriteLine("{0}狗会咬{1}",Name,PersonName);
        }
    }
}
