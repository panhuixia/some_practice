﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Program
    {
        static void Main(string[] args)
        {
       
            Console.WriteLine("请输入动物的类别：（狗/鱼）");
            string str = Console.ReadLine();

            if (str.Equals("狗"))
            {
                Console.WriteLine("请输入狗的名字");
                Dog dog = new Dog();
                dog.Name = Console.ReadLine();

                Console.WriteLine();

                Console.WriteLine("请输入狗的颜色");
                dog.Color = Console.ReadLine();

                dog.Bite();
            
            }
            if (str.Equals("鱼"))
            {

                Console.WriteLine("请输入鱼的名字");
                Fish fish = new Fish();
                fish.Name = Console.ReadLine();

                Console.WriteLine();
             

                Console.WriteLine("请输入鱼的颜色");
                fish.Color = Console.ReadLine();
                fish.Bite();
                
       

            
            }
          
            Console.WriteLine("结束");
        }
    }
}
