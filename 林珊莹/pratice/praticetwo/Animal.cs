﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace praticetwo
{
    public abstract class Animal
    {
        public string AnimalName { get; set; }

        public string AnimalColor { get; set; }

        public virtual void Move()
        {
            Console.WriteLine("动物可以移动");
        }
    }
}
