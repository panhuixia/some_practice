﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Dog:Animal
    {
        public Dog()
        {
            this.Name = "狗";
            Console.Write($"请输入{Name}的颜色：");
            this.Color = Console.ReadLine();
        }
        public void bite()
        {
            Console.WriteLine($"{Color}色的{Name}咬人了！");
        }
    }
}
