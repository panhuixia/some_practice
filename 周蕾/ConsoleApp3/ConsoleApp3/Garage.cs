﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
     class Garage
    {
        public string Name { get; } = "小周修车厂";
        public string Address { get; } = "你心里";
        public int Phone { get; } =4008820;

        public void garage()
        {
            Console.WriteLine("来到修车厂！");
            Console.WriteLine($"修车厂名称：{Name}\n修车厂地址：{Address}\n修车厂电话：{Phone}");
        }
    }
}
