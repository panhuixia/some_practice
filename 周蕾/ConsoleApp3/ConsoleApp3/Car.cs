﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Car
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int Wheel 
        {
            get
            {
                return Wheel;
            }
            set
            {
                if (value==4)
                {
                    Run();
                }
                else
                {
                    Garage garage = new Garage();
                    garage.garage();
                }
            }
        }
        public void Run()
        {
            Console.WriteLine($"{Color}色{Name}车跑起来了！");
        }
    }
}
