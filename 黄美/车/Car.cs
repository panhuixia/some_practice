﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class Car
    {
       public static string Name { get; set; }

       public static string Color { get; set; }

       public static void Move()
        {
            Console.WriteLine("车会行驶");
        }

       public int Wheel { get; set; }

        public virtual void Execute()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine("{0}的{1}有{2}个轮子，可正常行驶",Name,Color,Wheel);
            }
            else
            {
                Console.WriteLine( "{0}的{1}损坏了，需送去修车厂进行维修",Name,Color);
            }
        }
        
    }
}
