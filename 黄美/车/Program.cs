﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Car.Name = "玛莎拉蒂";
            Car.Color = "金色";
            car.Wheel = 3;

            Garage garage = new Garage();
            garage.GarageName = "4s店";
            garage.Address = "龙岩新罗区";
            garage.Tel = 12345678;
            car.Execute();

            garage.Execute();
      
        }
    }
}
