﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Fish : Animals
    {
        public override void Color()
        {
            Console.WriteLine("鱼是金色的");
        }

        public override void Move()
        {
            Console.WriteLine("鱼会游");
        }

        public override void Name()
        {
            Console.WriteLine("鱼的名字是雪儿");
        }
        public void Blow()
        {
            Console.WriteLine("鱼会吹泡泡");
        }
    }
}
