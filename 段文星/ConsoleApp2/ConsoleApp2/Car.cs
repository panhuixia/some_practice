﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
     class Car :Garage
    {
        public string CarName { set; get; }
        public string CarColor { set; get; }
        public int CarWheel { set; get; }
        public  void Run()
        {
            if (CarWheel >= 4)
                {
                Console.WriteLine("可以上路行驶");
                }
            else
            {
                Garage.Repair();
                Console.WriteLine("请输入修车厂的名字：");
                GarageName = Console.ReadLine();
                Console.WriteLine("请输入修车厂的电话：");
                GaragePhone = int.Parse(Console.ReadLine());
                Console.WriteLine("请输入修车厂的地址：");
                GarageAddress = Console.ReadLine();
            }
            Console.WriteLine("完成");
        }

    }
}
