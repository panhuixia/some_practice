﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Garage
    {
        public string GarageName { get; set; }
        public int  GaragePhone { get; set; }
        public string GarageAddress { get; set; }
        public static void Repair()
        {
            Console.WriteLine("送修车厂维修");
        }
    }
}
