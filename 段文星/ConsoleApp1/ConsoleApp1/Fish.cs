﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Fish : Animals
    {
        public override void Color()
        {
            Console.WriteLine("鱼是黑色的");
        }

        public override void Move()
        {
            Console.WriteLine("鱼会游泳");
        }

        public override void Name()
        {
            Console.WriteLine("鱼叫鱼");
        }
        public void Bubble()
        {
            Console.WriteLine("鱼会吐泡泡");
        }
    }
}
