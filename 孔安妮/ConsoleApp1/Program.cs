﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Program
    {
        static void Main(string[] args)
        {

            Dog dog = new Dog();
            Fish fish = new Fish();

            dog.Name();
            dog.Color();
            dog.Portable();

            fish.Name();
            fish.Color();
            fish.Portable();
        }
    }
}
