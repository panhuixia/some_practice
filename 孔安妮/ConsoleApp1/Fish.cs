﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Fish : Animals
    {
        public override void Color()
        {
            Console.WriteLine("鱼有颜色");
        }

        public override void Name()
        {
            Console.WriteLine("鱼有名字");
        }

        public override void Portable()
        {
            Console.WriteLine("鱼会游泳");
        }
    }
}
