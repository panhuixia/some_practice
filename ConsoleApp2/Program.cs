﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入动物是狗还是鱼：");
            string str = Console.ReadLine();
            if (str.Equals("狗"))
            {
                Dog dog = new Dog();
                Console.WriteLine("请输入狗的姓名：");
                dog.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入狗的颜色：");
                dog.AnimalColor = Console.ReadLine();
                dog.Remove();
            }
            if (str.Equals("鱼"))
            {
                Fish fish = new Fish();
                Console.WriteLine("请输入鱼的姓名：");
                fish.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入鱼的颜色：");
                fish.AnimalColor = Console.ReadLine();
                fish.Remove();
            }
            Console.WriteLine("完成");
        }
    }
}
