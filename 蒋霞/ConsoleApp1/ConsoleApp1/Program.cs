﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        //描述车、修车厂两个类，车具备名字、颜色、轮子数三个属性、车具备跑得行为，
        //车在跑的时候应该判断是否够四个轮子，如果够四个轮子可以跑起，否则送去修车厂维修。
        //修车厂具备，名字、地址、电话三个属性，具备修车的功能行为
        static void Main(string[] args)
        {
            Console.WriteLine("请输入车轮数：");
            Car car = new Car();
            car.Ctyre = int.Parse(Console.ReadLine());
            car.run();
        }
    }
}
