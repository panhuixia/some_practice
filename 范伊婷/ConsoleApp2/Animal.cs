﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class Animal
    {
        public string AnimalName { get; set; }

        public string AnimalColor { get; set; }

        public virtual void move()
        {
            Console.WriteLine("移动");
        }
    }
}
